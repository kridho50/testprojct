package com.example.messagetest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.messagetest.R;
import com.example.messagetest.message.ChatAdapter;
import com.example.messagetest.message.GetChatMassage;
import com.example.messagetest.model.SourceResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListNewsAdapter extends RecyclerView.Adapter<ListNewsAdapter.ViewHolder> {
    Context context;
    private List<SourceResponse.Source> sourceResponses = new ArrayList<>();

    public ListNewsAdapter(Context context){
      this.context = context;
    }

    public void setData(List<SourceResponse.Source> sourceResponses) {
        this.sourceResponses = sourceResponses;
    }
    @NonNull
    @Override
    public ListNewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListNewsAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListNewsAdapter.ViewHolder holder, int position) {
        holder.tvTittle.setText(sourceResponses.get(position).getName());
        holder.tvDesc.setText(sourceResponses.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return sourceResponses.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTittle, tvDesc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTittle = itemView.findViewById(R.id.tv_tittle);
            tvDesc = itemView.findViewById(R.id.tv_desc);
        }
    }
}
