package com.example.messagetest.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;

import com.example.messagetest.R;
import com.example.messagetest.adapter.ListNewsAdapter;
import com.example.messagetest.message.ChatAdapter;
import com.example.messagetest.message.MainActivity;
import com.example.messagetest.model.NewsResponse;
import com.example.messagetest.model.SourceResponse;
import com.example.messagetest.service.APIClient;
import com.example.messagetest.service.Api;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity {
    Api api;
    ListNewsAdapter listNewsAdapter;
    RecyclerView rvNews;
    List<SourceResponse.Source> sourceResponses = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        rvNews = findViewById(R.id.rv_list);
        getNews();
    }

    private void getNews(){
        String key = "5a78b282a4034cdaadfb2c564e6bd49e";
        api = APIClient.getClient().create(Api.class);
        Call<SourceResponse> call = api.getMediaNews(key);
        call.enqueue(new Callback<SourceResponse>() {
            @Override
            public void onResponse(Call<SourceResponse> call, Response<SourceResponse> response) {
                if(response.isSuccessful()){
                    sourceResponses = response.body().getSources();
                    setAdapterNews(sourceResponses);
                }
            }

            @Override
            public void onFailure(Call<SourceResponse> call, Throwable t) {

            }
        });
    }

    public void setAdapterNews(List<SourceResponse.Source> responses){
        listNewsAdapter = new ListNewsAdapter(NewsActivity.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvNews.setLayoutManager(layoutManager);
        rvNews.setAdapter(listNewsAdapter);
        listNewsAdapter.setData(responses);
    }
}