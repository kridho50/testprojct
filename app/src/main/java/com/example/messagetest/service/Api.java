package com.example.messagetest.service;

import com.example.messagetest.model.NewsResponse;
import com.example.messagetest.model.SourceResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    @GET("top-headlines/sources")
    Call<SourceResponse> getMediaNews(@Query("apiKey") String apiKey);

    @GET("everything")
    Call<NewsResponse> News(@Query("sources")String source,
                               @Query("apiKey") String apiKey);

}
